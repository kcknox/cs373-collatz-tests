#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)
        
    # my additional tests
    
    def test_read_my_1(self):
        s = "123456 999999\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  123456)
        self.assertEqual(j, 999999)
        
    def test_read_my_2(self):
        s = "501 501\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  501)
        self.assertEqual(j, 501)
        
    def test_read_my_3(self):
        s = "1 989898\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 989898)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)
        
    # my additional tests
    
    def test_eval_my_1(self):
        v = collatz_eval(1, 999999)
        self.assertEqual(v, 525)
        
    def test_eval_my_2(self):
        v = collatz_eval(700000, 800000)
        self.assertEqual(v, 504)
        
    def test_eval_my_3(self):
        v = collatz_eval(1942, 1942)
        self.assertEqual(v, 38)

    def test_eval_my_4(self):
        v = collatz_eval(350, 305)
        self.assertEqual(v, 144)

    def test_eval_my_5(self):
        v = collatz_eval(138, 138)
        self.assertEqual(v, 16)
    def test_eval_my_6(self):
        v = collatz_eval(9998, 9999)
        self.assertEqual(v, 92)
    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")
        
    # my additional tests
    
    def test_print_my_1(self):
        w = StringIO()
        collatz_print(w, 1, 999999, 525)
        self.assertEqual(w.getvalue(), "1 999999 525\n")
        
    def test_print_my_2(self):
        w = StringIO()
        collatz_print(w, 700000, 800000, 504)
        self.assertEqual(w.getvalue(), "700000 800000 504\n")
    
    def test_print_my_3(self):
        w = StringIO()
        collatz_print(w, 1942, 1942, 38)
        self.assertEqual(w.getvalue(), "1942 1942 38\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    # my additional tests
    
    def test_solve_my_1(self):
        r = StringIO("1 999999\n700000 800000\n1942 1942\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 999999 525\n700000 800000 504\n1942 1942 38\n")
            
    def test_solve_my_2(self):
        r = StringIO("321 521\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "321 521 144\n")
            
    def test_solve_my_3(self):
        r = StringIO("1 2\n3 4\n10 100\n100 200\n201 301\n1000 2000\n3001 5000\n40000 50000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 2 2\n3 4 8\n10 100 119\n100 200 125\n201 301 128\n1000 2000 182\n3001 5000 238\n40000 50000 314\n")
    
    
# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
